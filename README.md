# Foundation Titles Hand
Hand-drawn font of the character design used in the title sequence of the Foundation series.

Foundation, the Apple TV+ show made from the Isaac Asimov books (of which only four episodes had been released when this font was first published), uses a number of fonts with related design for the logo, on-screen text, in-scene signage and the title credits.  Only in the last of these are most letters represented.

Foundation’s title sequence was created by Imaginary Forces, a Los Angeles design studio and creative agency (imaginaryforces.com), and presumably IF created the specific design used for names in the title credits.  In this font, I have hand-drawn the characters seen on-screen, excepting Q and X, which do not appear; those are my invention.  Lowercase letters, numbers, and symbols and punctuation (other than & and . and ,) are also not used, hence their omission here.

The source .archive file is the file format used by iFontMaker, the iPad font-design tool made by Eiji and Tom, Inc., which you can find at 2ttf.com.

This font is released under the OFL, the SIL Open Font License.
